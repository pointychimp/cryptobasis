#ifndef BASIS_H
#define BASIS_H
#include <vector>
#include <algorithm>
#include <string>
#include "datetime.h"
#include "numstr.h"

namespace Basis
{

    struct Input
    {
        unsigned int id;
        DateTime date;
        std::string exchange;
        std::string txID;
        std::string notes;
        int64_t cost;
        int64_t fee;
        int64_t amount;
        Input(std::string _date, int64_t _amount, int64_t _cost) :
            date  (DateTime(_date)),
            cost  (_cost),
            fee   (0),
            amount(_amount) {}
        Input();
    };
    struct InputReference
    {
        Input* in;
        int64_t amount;
        InputReference(Input* _in, int64_t _amount) : in(_in),     amount(_amount) {}
        InputReference()                            : in(nullptr), amount(0)       {}
    };
    struct Output
    {
    public:
        unsigned int id;
        DateTime date;
        std::string exchange;
        std::string txID;
        std::string notes;
        int64_t proceeds;
        int64_t amount;
        int64_t gains();
        std::vector<InputReference> inputs;

        unsigned int numRefs() {return inputs.size();}

        Output(std::string _date, int64_t _amount, int64_t _proceeds) :
            date     (DateTime(_date)),
            proceeds (_proceeds),
            amount   (_amount) {}
        Output();
    };

    enum enumBTCunit {BTC, mBTC, uBTC, sat};
    std::string listBTCUnits();

    extern int timeZone;

    bool operator< (const Input& a, const Input& b);
    bool operator> (const Input& a, const Input& b);
    bool operator<=(const Input& a, const Input& b);
    bool operator>=(const Input& a, const Input& b);
    bool operator==(const Input& a, const Input& b);
    bool operator!=(const Input& a, const Input& b);
    bool operator< (const Output& a, const Output& b);
    bool operator> (const Output& a, const Output& b);
    bool operator<=(const Output& a, const Output& b);
    bool operator>=(const Output& a, const Output& b);
    bool operator==(const Output& a, const Output& b);
    bool operator!=(const Output& a, const Output& b);

    class Basis
    {
    private:
        std::vector<Input> inputs;
        std::vector<Output> outputs;
        bool needRef;
        enumBTCunit preferredUnit;
        std::string  preferredUnitString;
        int64_t sum(std::vector<Input> in);
        int64_t sum(std::vector<Output> out);

    public:
        void          addInput(Input in);
        void          addInput(Input *in);
        void          addOutput(Output out);
        void          addOutput(Output *out);
        void          resetAll();
        void          setRefresh(bool v=true)        {needRef = v;}
        unsigned int  getNumInputs()                 {return inputs.size();}
        unsigned int  getNumOutputs()                {return outputs.size();}
        void          removeInput(unsigned int i)    {inputs.erase(inputs.begin()+i); setRefresh();}
        void          removeOutput(unsigned int i)   {outputs.erase(outputs.begin()+i); setRefresh();}
        Input*        getInput(unsigned int i)       {return &inputs.at(i);}
        Output*       getOutput(unsigned int i)      {return &outputs.at(i);}
        int64_t       balance()                      {return sum(inputs)-sum(outputs);}
        int64_t       gains();
        enumBTCunit   getPreferredUnit()             {return preferredUnit;}
        std::string   getPreferredUnitString()       {return preferredUnitString;}
        void          setPreferredUnit(enumBTCunit u);
        void          refresh(bool force = false);
        Basis();
        ~Basis();
    };

    std::ostream&  operator<<(std::ostream&  os, Basis  basis);
    std::ostream&  operator<<(std::ostream&  os, Input  input);
    std::ostream&  operator<<(std::ostream&  os, Output output);
    std::ifstream& operator>>(std::ifstream& is, Basis*  basis);
};

#endif // BASIS_H
