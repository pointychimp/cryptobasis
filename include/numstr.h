/*
 * Matt Traudt mt.traudt@gmail.com
 * bitcoin:1AJrWvgLxLocB3uVvwThKkTdHyGLWmJfm
 * See my other utilities: https://bitbucket.org/pointychimp/utilities-c.git
 *
 * last modified: 2014-03-05
 *
 * README ----------------------------------------------------------------------
 *
 * namespace NumStr
 * files: numstr.h, numstr.cpp
 *
 * dependencies: none
 *
 * This namespace allows you to convert integers to strings.
 * The integer in question can optionally have digits on the end
 * representing digits after a decimal.
 *
 * This namespace could include more str <--> num conversions in the
 * future if I find a need for them.
 *
 * LICENSE ---------------------------------------------------------------------
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NUMSTR_H
#define NUMSTR_H
#include <string>

namespace NumStr
{
    /*
     * Functions shall be named in the following fashion.
     * destinationType sourceType_destinationType(sourceType val, ... );
    */
    /*
     * warning: this function will truncate, not round, if needed
     *
     * val: value to convert from int to str
     * decimals: optional number of digits starting on rhs that are supposed
     *           to be on rhs of decimal point
     * minDisplayDecimals: required minimum number of digits  to rhs
     *                     of decimal point (adds trailing zeros if needed).
     * maxDisplayDecimals: optional maximun number of digits allowed to
     *                     the right of decimal point. Truncates!
     * minDisplayWholes: optional required minimum numbers of digits to the
     *                   left of the decimal point (adds leading zeros if
     *                   necessary). 0 = no leading zero if < 1 (.1234 not
     *                   0.1234), otherwise will add leading zeros as needed.
    */
    std::string int_str(int64_t val, unsigned int decimals = 0,
                        unsigned int minDisplayDecimals = 0, unsigned int maxDisplayDecimals = 1000,
                        unsigned int minDisplayWholes = 1);
    /*
     * warning: this function will only handle positive/negative integers,
     * It will not understand decimals in the string or leading zeros.
     * "-001234" returns -1234, 1234.5 is invalid (returns 0).
    */
    int64_t str_int(std::string val);
};

#endif // NUMSTR_H
