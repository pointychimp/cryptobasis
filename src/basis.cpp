#include "../include/basis.h"

#include <sstream>
#include "../include/numstr.h"
#include <fstream>
#include <iostream>

namespace Basis
{

    int timeZone;

    Input::Input()
    {
        date = DateTime("20090103181505");
        exchange = txID = notes = "";
        cost = fee = amount = 0;
    }
    Output::Output()
    {
        date = DateTime("20090103181505");
        exchange = txID = notes = "";
        proceeds = amount = 0;
    }
    int64_t Output::gains()
    {
        int64_t totCost = 0;
        for (unsigned int i = 0; i < inputs.size(); i++)
        {
            totCost +=
                (inputs[i].in->cost+inputs[i].in->fee) * (double)inputs[i].amount/inputs[i].in->amount;
        }
        return this->proceeds - totCost;
    }

    std::string listBTCUnits() {return "0 = BTC\n1 = mBTC\n2 = uBTC\n3 = sat\n";}

    bool operator< (const Input& a,  const Input& b)  {return a.date <  b.date;}
    bool operator==(const Input& a,  const Input& b)  {return a.date == b.date;}
    bool operator> (const Input& a,  const Input& b)  {return   b <  a ;}
    bool operator<=(const Input& a,  const Input& b)  {return !(a >  b);}
    bool operator>=(const Input& a,  const Input& b)  {return !(a <  b);}
    bool operator!=(const Input& a,  const Input& b)  {return !(a == b);}
    bool operator< (const Output& a, const Output& b) {return a.date <  b.date;}
    bool operator==(const Output& a, const Output& b) {return a.date == b.date;}
    bool operator> (const Output& a, const Output& b) {return   b <  a ;}
    bool operator<=(const Output& a, const Output& b) {return !(a >  b);}
    bool operator>=(const Output& a, const Output& b) {return !(a <  b);}
    bool operator!=(const Output& a, const Output& b) {return !(a == b);}

    int64_t Basis::sum(std::vector<Input> in)
    {
        int64_t total = 0;
        for (unsigned int i = 0; i < in.size(); i++)
        {
            total += in.at(i).amount;
        }
        return total;
    }
    int64_t Basis::sum(std::vector<Output> out)
    {
        int64_t total = 0;
        for (unsigned int i = 0; i < out.size(); i++)
        {
            total += out.at(i).amount;
        }
        return total;
    }

    void Basis::addInput(Input in)
    {
        inputs.push_back(in);
        setRefresh(true);
    }
    void Basis::addInput(Input *in)
    {
        Input newInput = *in;
        addInput(newInput);
    }
    void Basis::addOutput(Output out)
    {
        outputs.push_back(out);
        setRefresh(true);
    }
    void Basis::addOutput(Output *out)
    {
        Output newOutput = *out;
        addOutput(newOutput);
    }

    int64_t Basis::gains()
    {
        int64_t totGains = 0;
        for (unsigned int i = 0; i < outputs.size(); i++)
            {totGains += outputs[i].gains();}
        return totGains;
    }

    void Basis::resetAll()
    {
        inputs.resize(0);
        outputs.resize(0);
        //setPreferredUnit(mBTC);

        timeZone = 0;
        setRefresh(false);
    }

    void Basis::refresh(bool force)
    {
        if (needRef || force)
        {
            std::sort(inputs.begin(), inputs.end());
            std::sort(outputs.begin(), outputs.end());

            for (unsigned int i = 0; i < inputs.size(); i++) {inputs[i].id = i;}
            for (unsigned int i = 0; i < outputs.size(); i++) {outputs[i].id = i;}

            if (sum(inputs) < sum(outputs))
                ;//throw 1;

            if (inputs.size() > 0)
            {
                if (outputs.size() > 0)
                {
                    // remove all input references in outputs
                    for (unsigned int i = 0; i < outputs.size(); i++)
                        {outputs.at(i).inputs.clear();}

                    unsigned int currentInput = 0;
                    Input* activeInput = &inputs.at(currentInput);
                    int64_t actInRemaining = activeInput->amount;
                    int64_t stillNeed;
                    for (unsigned int i = 0; i < outputs.size(); i++)
                    {
                        stillNeed = outputs.at(i).amount;
                        while (stillNeed > actInRemaining)
                        {
                            stillNeed -= actInRemaining;
                            outputs.at(i).inputs.push_back(InputReference(activeInput, actInRemaining));
                            activeInput = &inputs.at(++currentInput);
                            actInRemaining = activeInput->amount;
                            //std::cout << "o" << i << " eats i" << currentInput << std::endl;
                        }
                        outputs.at(i).inputs.push_back(InputReference(activeInput, stillNeed));
                        actInRemaining -= stillNeed;
                        if (actInRemaining <= 0 && currentInput + 1 < inputs.size())
                        {
                            activeInput = &inputs.at(++currentInput);
                            actInRemaining = activeInput->amount;
                        }
                        //std::cout << "o" << i << " finishes with i" << currentInput << " (remaining " << actInRemaining << ")" << std::endl;
                    }
                    //std::cout << "----------" << std::endl;
                }
                else
                {
                    // no outputs, nothing to calculate
                }

            }
            else
            {
                // no inputs, nothing to calculate
            }


        }
        setRefresh(false);
    }
    void Basis::setPreferredUnit(enumBTCunit u)
    {
        preferredUnit = u;
        if      (preferredUnit ==  BTC) preferredUnitString =  "BTC";
        else if (preferredUnit == mBTC) preferredUnitString = "mBTC";
        else if (preferredUnit == uBTC) preferredUnitString = "uBTC";
        else                            preferredUnitString =  "sat";

    }
    Basis::Basis()
    {
        timeZone = 0;
        setPreferredUnit(BTC);

        setRefresh(true);
    }
    Basis::~Basis()
    {

    }

    std::ostream &operator<<(std::ostream &os, Basis basis)
    {
        os << "WARNING: do not edit this file directly" << std::endl;
        os << basis.getPreferredUnit() << std::endl;
        os << "---inputs---" << std::endl;
        for (unsigned int i = 0; i < basis.getNumInputs(); i++)
        {
            os << *basis.getInput(i);
        }
        os << "---/inputs---" << std::endl;
        os << "---outputs---" << std::endl;
        for (unsigned int i = 0; i < basis.getNumOutputs(); i++)
        {
            os << *basis.getOutput(i);
        }
        os << "---/outputs---" << std::endl;
        return os;
    }
    std::ostream &operator<<(std::ostream &os, Input input)
    {
        //os << input.id << std::endl;
        os << input.date.format("%Y%M%D%H%I%S") << std::endl;
        os << input.amount << std::endl;
        os << input.cost << std::endl;
        os << input.fee << std::endl;
        os << (input.txID     != "" ? input.txID     : "null") << std::endl;
        os << (input.notes    != "" ? input.notes    : "null") << std::endl;
        os << (input.exchange != "" ? input.exchange : "null") << std::endl;
        return os;
    }
    std::ostream &operator<<(std::ostream &os, Output output)
    {
        os << output.date.format("%Y%M%D%H%I%S") << std::endl;
        os << output.amount << std::endl;
        os << output.proceeds << std::endl;
        os << (output.txID     != "" ? output.txID     : "null") << std::endl;
        os << (output.notes    != "" ? output.notes    : "null") << std::endl;
        os << (output.exchange != "" ? output.exchange : "null") << std::endl;
        return os;
    }
    std::ifstream &operator>>(std::ifstream &is, Basis* basis)
    {
        enum ErrorEnum {NOINPUTS, BADDATEFORMAT, NOOUTPUTS};
        try
        {
            basis->resetAll();
            std::string bufStr;
            getline(is, bufStr); // warning
            getline(is, bufStr); // preferred unit as int in a string
            basis->setPreferredUnit((enumBTCunit)NumStr::str_int(bufStr));
            getline(is, bufStr);
            if (bufStr != "---inputs---") throw NOINPUTS;
            getline(is, bufStr);
            while (bufStr != "---/inputs---")
            {
                Input pendingInput;
                pendingInput.date = DateTime(bufStr);
                if (!pendingInput.date.isValid()) throw BADDATEFORMAT;
                is >> pendingInput.amount;
                is >> pendingInput.cost;
                is >> pendingInput.fee;
                is >> pendingInput.txID;
                is.clear(); is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                getline(is, pendingInput.notes);
                getline(is, pendingInput.exchange);
                if (pendingInput.txID     == "null") pendingInput.txID     = "";
                if (pendingInput.notes    == "null") pendingInput.notes    = "";
                if (pendingInput.exchange == "null") pendingInput.exchange = "";
                basis->addInput(pendingInput);
                getline(is, bufStr);
            }
            getline(is, bufStr);
            if (bufStr != "---outputs---") throw NOOUTPUTS;
            getline(is, bufStr);
            while (bufStr != "---/outputs---")
            {
                Output pendingOutput;
                pendingOutput.date = DateTime(bufStr);
                if (!pendingOutput.date.isValid()) throw BADDATEFORMAT;
                is >> pendingOutput.amount;
                is >> pendingOutput.proceeds;
                is >> pendingOutput.txID;
                is.clear(); is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                getline(is, pendingOutput.notes);
                getline(is, pendingOutput.exchange);
                if (pendingOutput.txID     == "null") pendingOutput.txID     = "";
                if (pendingOutput.notes    == "null") pendingOutput.notes    = "";
                if (pendingOutput.exchange == "null") pendingOutput.exchange = "";
                basis->addOutput(pendingOutput);
                getline(is, bufStr);
            }
            basis->refresh(true);
        }
        catch (int e)
        {
            if (e == NOINPUTS) std::cout << "No inputs. Stopped processing file.\n";
            if (e == BADDATEFORMAT) std::cout << "Bad date format. THIS IS BAD. Stopped processing file.\n";
            if (e == NOOUTPUTS) std::cout << "No outputs. Stopped processing file.\n";
        }
        catch (std::exception e)
        {
            //std::cout << e.what() << std::endl;
            //std::cout << "If this is a new file, this is normal. Otherwise, there's something wrong.\n";
        }
        return is;
    }
}
