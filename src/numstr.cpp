#include "../include/numstr.h"
#include <sstream>
#include <cmath>

//#include <exception>

/*class RangeException : public std::exception
{
    virtual const char* what() const throw()
        {return "max < min display decimals and may cause unexpected behavior.";}
} rangeException;*/

// only works for positive unsigned int powers!
int64_t pow(int64_t base, unsigned int exp)
{
    if (exp > 0) return base * pow(base, exp-1);
    else return 1;
}

std::string NumStr::int_str(int64_t val, unsigned int decimals,
                            unsigned int minDisplayDecimals, unsigned int maxDisplayDecimals,
                            unsigned int minDisplayWholes)
{
    //if (maxDisplayDecimals < minDisplayDecimals) throw rangeException;
    std::stringstream ss;
    bool isPositive = (val >= 0);
    int64_t factor = pow(10,decimals); // my custom power function for integers required
    int64_t wholePart = std::abs(val / factor);
    int64_t fractPart = std::abs(val % factor);
    ss << wholePart;
    if (factor > 0 && fractPart > 0)
    {
        ss << ".";
        double numLeadingZeros = log10((double)factor/fractPart) - 1;
        while (numLeadingZeros-- > 0) {ss << "0";}
        ss << fractPart;
    }
    std::string str = ss.str();
// work with rhs
    if (minDisplayDecimals > 0 && // want decimals no matter what
        str.find(".") != std::string::npos && // there are some already
        str.substr(str.find(".")+1, str.length()-1).length() < minDisplayDecimals) // and there isn't enough
    {
        str = str.substr(0, str.find(".")) + // lhs
              "." + // decimal
              str.substr(str.find(".")+1) + // rhs
              std::string(minDisplayDecimals - str.substr(str.find(".")+1).length(), '0'); // extra zeros
    }
    else if (minDisplayDecimals > 0 && // want decimals no matter what
             str.find(".") == std::string::npos) // there are not any yet
    {
        str += "." + std::string(minDisplayDecimals, '0');
    }
    if (str.find(".") != std::string::npos && // there is a rhs
        str.substr(str.find(".")+1).length() > maxDisplayDecimals) // rhs too long
    {
        str = str.substr(0, str.find(".")) + // lhs
              "." + // decimal
              str.substr(str.find(".")+1, maxDisplayDecimals); // rhs truncated
    }
// work with lhs
    if (str.substr(0, str.find(".")).length() < minDisplayWholes) // not enough lhs digits
    {
        str = std::string(minDisplayWholes - str.substr(0, str.find(".")).length(), '0')  + str;
    }
    else if (str.substr(0, str.find(".")) == "0" && minDisplayWholes == 0) // don't want leading zeros for val < 1
    {
        str = str.substr(str.find("."));
    }
    if (!isPositive) str = "-" + str;
    return str;
}

int64_t NumStr::str_int(std::string val)
{
    if (val.find_first_not_of("0123456789-") != std::string::npos)
        {return 0;} // invalid character anywhere
    if (val.substr(1).find_first_not_of("0123456789") != std::string::npos)
        {return 0;} // negative sign somewhere other than front
    bool isPositive = true;
    if (val.at(0) == '-') {isPositive = false; val = val.substr(1);}
    int64_t value = 0;
    int pos = val.length() - 1;
    int64_t factor = 1;
    while (pos >= 0)
    {
        switch (val.at(pos))
        {
            case '1': value += 1*factor; break; case '2': value += 2*factor; break;
            case '3': value += 3*factor; break; case '4': value += 4*factor; break;
            case '5': value += 5*factor; break; case '6': value += 6*factor; break;
            case '7': value += 7*factor; break; case '8': value += 8*factor; break;
            case '9': value += 9*factor; break; case '0': value += 0*factor; break;
            //default: return 0; // non-numeric character found
        };
        pos--;
        factor *= 10;
    }
    if (!isPositive) {value *= -1;}
    return value;
}

