#include <iostream>
#include <string>
#include <sstream>
#include <limits>
#include <algorithm>
#include <fstream>
#include "include/basis.h"
#include "include/numstr.h"

using namespace std;

const string softwareName = "Crypto Basis";
const string softwareVersion = "1.3.1";
string currentFile = "";
Basis::Basis currentBasis;

// reads single character from std::cin, then
// removes extra from buffer and makes sure char is lowercase
void getCharInput(char* c)
{
    std::cin.get(*c);
    std::cin.clear();
    if (*c != '\n') std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    *c = tolower(*c);
}
char getCharInput()
{
    char c;
    std::cin.get(c);
    std::cin.clear();
    if (c != '\n') std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return tolower(c);
}

// options such as new, open, etc.
void fileMenu();
// options such as adding inputs/outputs,
// saving, calculating things, etc.
void mainMenu();
// prompts user for a filename and creates file
void newFile();
// prompts user for a filename.
// sets filename to currentFile.
// loads currentFile into currentBasis.
// catches any errors and tells user about them.
void openFile();
// display all the inputs in a basis
void displayInputs();
// display all int outputs in a basis
void displayOutputs();
// prints all the info about an input
void aboutInput(Basis::Input* input);
// prints all the info about an output
void aboutOutput(Basis::Output* output);
// menu for putting together an input
// argument is a new input if building from scratch,
// otherwise we're editing as the process would be the same
void editInput(Basis::Input* input);
// menu for putting together an output
// argument is a new output if building from scratch,
// otherwise we're editing as the process would be the same
void editOutput(Basis::Output* output);
// goes through each configurable option
// displays the current value, and prompts the user to change
void editOptions();
// exports information to a csv that is very helpful
// in preparing to fill out a Form 8949
void exportToCSV();
// less useful for taxes, but shows more information
// that may be helpful for debugging or maybe personal
// finance reasons
void exportToVerboseCSV();

int main(int argc, char* argv[])
{
    fileMenu();
    if (currentFile != "")
    {
        // load file into currentBasis
        ifstream iFile(currentFile);
        // todo: error check
        iFile >> &currentBasis;
        iFile.close();
        // go to mainMenu
        mainMenu();
    }
}

void fileMenu()
{
    char input;
    cout << softwareName << " v. " << softwareVersion << endl;
    cout << "Current file: " << (currentFile!=""?currentFile:"none") << endl;
    do
    {
        cout << "\nWhat would you like to do?" << endl;
        cout << "1. Create [n]ew file\n2. [O]pen existing file\n";
        cout << "3. E[x]it\n>";
        getCharInput(&input);
        switch (input)
        {
        case '1':case 'n': newFile(); break;
        case '2':case 'o': openFile(); break;
        case '3':case 'x': break;
        default:
            cout << "Invalid option\n";
            input = '\n';
        }
    } while (input == '\n');
    cout << endl;
}

void mainMenu()
{
    using namespace NumStr;
    char mainMenuSelection;
    string inputStr;
    while (mainMenuSelection != 's' &&
           mainMenuSelection != '7' &&
           mainMenuSelection != 'x' &&
           mainMenuSelection != '8')
    {
        currentBasis.refresh();
        string balanceStr;
        switch (currentBasis.getPreferredUnit())
        {
        case Basis::enumBTCunit::BTC:
            balanceStr = int_str(currentBasis.balance(), 8) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::mBTC:
            balanceStr = int_str(currentBasis.balance(), 5) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::uBTC:
            balanceStr = int_str(currentBasis.balance(), 2) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::sat:
            balanceStr = int_str(currentBasis.balance(), 0) + " " + currentBasis.getPreferredUnitString(); break;
        }
        string gainsStr = (currentBasis.gains()<0?string("-"):"") + "$" + int_str(abs(currentBasis.gains()), 8, 2,2);
        cout << string((43-currentFile.length())/2, ' ') << currentFile << /*string((33-currentFile.length())/2, ' ') <<*/ endl;
        cout << "|-----------------------------------------|\n";
        cout << "| Current Balance " << string(42-20-balanceStr.length(), ' ') << " " << balanceStr << " |" << endl;
        cout << "| Total Gain/Loss " << string(42-20-gainsStr.length(), ' ') << " " << gainsStr << " |" << endl;
        cout << "|-----------------------------------------|\n";
        cout << endl;
        cout << "1. View [i]nputs\t2. View [o]utputs\n";
        cout << "3. Add new i[n]put\t4. Add new o[u]tput\n";
        cout << "5. [E]xport to csv\n";
        cout << "6. O[p]tions\t\t7. [S]ave and exit\n";
        cout << "8. E[x]it\n>";
        getCharInput(&mainMenuSelection);
        switch (mainMenuSelection)
        {
        case '1':case 'i': {
            if (currentBasis.getNumInputs() < 1) {cout << "No inputs\n"; break;}
            displayInputs();
            cout << "Input id of an input to see or do more with it, or press enter to continue.\n>";
            getline(cin, inputStr);
            if (inputStr != "" && inputStr.find_first_not_of("0123456789") == std::string::npos)
            {
                unsigned int inID = str_int(inputStr);
                if (inID >= currentBasis.getNumInputs()) {cout << "no input\n"; break;}
                cout << endl;
                aboutInput(currentBasis.getInput(inID));
                cout << "Want to [e]dit or [d]elete this input?\n>";
                char inputSelection = getCharInput();
                if (inputSelection == 'e')
                {
                    editInput(currentBasis.getInput(inID));
                    currentBasis.setRefresh();
                    cout << "\nPress enter to continue ... ";
                    cin.get();
                }
                else if (inputSelection == 'd')
                {
                    currentBasis.removeInput(inID);
                    cout << "\nRemoved.\nPress enter to continue ... ";
                    cin.get();
                }
            }
            else {cout << "no input\n"; break;}
            break; }
        case '2':case 'o': {
            if (currentBasis.getNumOutputs() < 1) {cout << "No outputs\n"; break;}
            displayOutputs();
            cout << "Input id of an output to see or do more with it, or press enter to continue.\n>";
            getline(cin, inputStr);
            if (inputStr != "" && inputStr.find_first_not_of("0123456789") == std::string::npos)
            {
                unsigned int outID = str_int(inputStr);
                if (outID >= currentBasis.getNumOutputs()) {cout << "no output\n"; break;}
                cout << endl;
                aboutOutput(currentBasis.getOutput(outID));
                cout << "Want to [e]dit or [d]elete this output?\n>";
                char outputSelection = getCharInput();
                if (outputSelection == 'e')
                {
                    editOutput(currentBasis.getOutput(outID));
                    currentBasis.setRefresh();
                    cout << "Press enter to continue ... ";
                    cin.get();
                }
                else if (outputSelection == 'd')
                {
                    currentBasis.removeOutput(outID);
                    cout << "\nRemoved.\nPress enter to continue ... ";
                    cin.get();
                }
            }
            else {cout << "no output\n"; break;}
            break; }
        case '3':case 'n': {
            Basis::Input pendingInput;
            char inputChar = 'e';
            editInput(&pendingInput);
            cout << endl;
            while (inputChar != '1' && inputChar != 'a' && inputChar != '3' && inputChar != 'q')
            {
                cout << "Pending Input: \n";
                aboutInput(&pendingInput);
                cout << "\n1. [a]dd\n2. [E]dit\n3. [q]uit\n>";
                getCharInput(&inputChar);
                if (inputChar == '2' || inputChar == 'e' || inputChar == '\n')
                    {editInput(&pendingInput);}
            }
            if (inputChar == '1' || inputChar == 'a') {currentBasis.addInput(pendingInput);}
            break; }
        case '4':case 'u': {
            Basis::Output pendingOutput;
            char inputChar = 'e';
            editOutput(&pendingOutput);
            while (inputChar != '1' && inputChar != 'a' && inputChar != '3' && inputChar != 'q')
            {
                cout << "\n1. [a]dd\n2. [E]dit\n3. [q]uit\n>";
                getCharInput(&inputChar);
                if (inputChar == '2' || inputChar == 'e' || inputChar == '\n')
                    {editOutput(&pendingOutput);}
            }
            if (inputChar == '1' || inputChar == 'a') {currentBasis.addOutput(pendingOutput);}
            break; }
        case '5':case 'e': {
            exportToCSV();
            break; }
        case '6':case 'p': {
            editOptions();
            break;}
        case '7':case 's': {
            ofstream oFile(currentFile);
            if (oFile.is_open())
                {oFile << currentBasis;}
            break; }
        case '8':case 'x':
            //exit();
            break;
        default:
            cout << "Invalid option\n";
        }
        cout << "\n";
    }
}

void newFile()
{
    cout << "\nWhat would you like to call the new file?\n";
    cout << "Warning: if it already exists, it will be\n";
    cout << "overwritten immediately. [cryptobasis.cbd]\n>";
    string filename;
    getline(cin, filename);
    if (filename=="") filename = "cryptobasis.cbd";
    currentFile = filename;
    ofstream oFile(filename);
    if (oFile.is_open())
    {
        oFile << Basis::Basis();
    }
}

void openFile()
{
    cout << "\nWhat is the name of the file you would like to open?\n";
    cout << "[cryptobasis.cbd]\n>";
    string filename;
    getline(cin, filename);
    if (filename=="") filename = "cryptobasis.cbd";
    currentFile = filename;
    ifstream iFile;
    iFile.open(currentFile);
    if (!iFile.is_open()) {cout << "File does not exist\n";}
    else
    {
        try {iFile >> &currentBasis;}
        catch (exception e) {cout << e.what() << endl;}
    }
    iFile.close();
}

void displayInputs()
{
    using NumStr::int_str;
    string titleStr = "INPUTS";
    //cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    cout << "  |-------------------------------------------------------------------|\n";
    cout << "  |" << string(34-titleStr.length()/2, ' ') << titleStr << string(33-titleStr.length()/2, ' ') << "|\n";
    cout << "  |-------------------------------------------------------------------|\n";
    cout << "  |  id  |    date    |   txid   |       amount       |  cost w/fees  |\n";
    cout << "  |------|------------|----------|--------------------|---------------|\n";
    for (int i=currentBasis.getNumInputs()-1; i >= 0; i--)
    {
        Basis::Input* input = currentBasis.getInput(i);
        string idStr = int_str(input->id, 0, 0,0, 4);
        string dateStr = input->date.format("%Y/%M/%D");
        string txid = input->txID.substr(0,8); // get first seven chars of txid
        string amountStr;
        switch (currentBasis.getPreferredUnit())
        {
        case Basis::enumBTCunit::BTC:
            amountStr = int_str(input->amount, 8, 8,8) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::mBTC:
            amountStr = int_str(input->amount, 5, 5,5) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::uBTC:
            amountStr = int_str(input->amount, 2, 2,2) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::sat:
            amountStr = int_str(input->amount, 0, 0,0) + " " + currentBasis.getPreferredUnitString(); break;
        }
        string totCostStr = "$" + int_str(input->cost+input->fee, 8, 2,2);
        cout << "  | " << idStr << " |";
        cout << " " << dateStr << " |";
        cout << " " << string(8-txid.length(), ' ') << txid << " |";
        cout << " " << string(18-amountStr.length(), ' ') << amountStr << " |"; // todo: error check and display *** if too long
        cout << " " << string(13-totCostStr.length(), ' ') << totCostStr << " |"; // todo: error check and display *** if too long
        cout << endl;
    }
    cout << "  |-------------------------------------------------------------------|\n";
}

void displayOutputs()
{
    using NumStr::int_str;
    string titleStr = "OUTPUTS";
    cout << "  |------------------------------------------------------------------------|\n";
    cout << "  | " << string(35-titleStr.length()/2, ' ') << titleStr << string(35-titleStr.length()/2, ' ') << "|\n";
    cout << "  |------------------------------------------------------------------------|\n";
    cout << "  |  id  |    date    |       amount       |    proceeds   |     gains     |\n";
    cout << "  |------|------------|--------------------|---------------|---------------|\n";
    for (int i=currentBasis.getNumOutputs()-1; i >= 0; i--)
    {
        Basis::Output* output = currentBasis.getOutput(i);
        string idStr = int_str(output->id, 0, 0,0, 4);
        string dateStr = output->date.format("%Y/%M/%D");
        string txid = output->txID.substr(0,8); // first seven chars of txid
        string amountStr;
        switch (currentBasis.getPreferredUnit())
        {
        case Basis::enumBTCunit::BTC:
            amountStr = int_str(output->amount, 8, 8,8) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::mBTC:
            amountStr = int_str(output->amount, 5, 5,5) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::uBTC:
            amountStr = int_str(output->amount, 2, 2,2) + " " + currentBasis.getPreferredUnitString(); break;
        case Basis::enumBTCunit::sat:
            amountStr = int_str(output->amount, 0, 0,0) + " " + currentBasis.getPreferredUnitString(); break;
        }
        string proceedsStr = "$" + int_str(output->proceeds, 8, 2,2);
        string gainsStr = (output->gains()<0?string("-"):"") + "$" + int_str(abs(output->gains()), 8, 2,2);
        cout << "  | " << idStr << " |";
        cout << " " << dateStr << " |";
        cout << " " << string(18-amountStr.length(), ' ') << amountStr << " |"; // todo: error check and display *** if too long
        cout << " " << string(13-proceedsStr.length(), ' ') << proceedsStr << " |"; // todo: error check and display *** if too long
        cout << " " << string (13-gainsStr.length(), ' ') << gainsStr << " |"; // todo: error check and display *** if too long
        cout << endl;
    }
    cout << "  |------------------------------------------------------------------------|\n";
}

void aboutInput(Basis::Input* input)
{
    using NumStr::int_str;
    string amountStr;
    switch (currentBasis.getPreferredUnit())
    {
    case Basis::enumBTCunit::BTC:
        amountStr = int_str(input->amount, 8, 8,8) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::mBTC:
        amountStr = int_str(input->amount, 5, 5,5) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::uBTC:
        amountStr = int_str(input->amount, 2, 2,2) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::sat:
        amountStr = int_str(input->amount, 0, 0,0) + " " + currentBasis.getPreferredUnitString(); break;
    }
    cout << "date:     " << input->date.format("%Y-%M-%D %H:%I:%S") << endl;
    cout << "amount:   " << amountStr << endl;
    cout << "cost:     $" << int_str(input->cost, 8, 8,8) << endl;
    cout << "fee:      $" << int_str(input->fee, 8, 8,8) << endl;
    cout << "exchange: " << input->exchange << endl;
    cout << "tx ID:    " << input->txID << endl;
    cout << "notes:    " << input->notes << endl;
}

void aboutOutput(Basis::Output* output)
{
    using NumStr::int_str;
    string amountStr;
    switch (currentBasis.getPreferredUnit())
    {
    case Basis::enumBTCunit::BTC:
        amountStr = int_str(output->amount, 8, 8,8) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::mBTC:
        amountStr = int_str(output->amount, 5, 5,5) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::uBTC:
        amountStr = int_str(output->amount, 2, 2,2) + " " + currentBasis.getPreferredUnitString(); break;
    case Basis::enumBTCunit::sat:
        amountStr = int_str(output->amount, 0, 0,0) + " " + currentBasis.getPreferredUnitString(); break;
    }
    cout << "date:      " << output->date.format("%Y-%M-%D %H:%I:%S") << endl;
    cout << "amount:    " << amountStr << endl;
    cout << "proceeds:  " << "$" << int_str(output->proceeds, 8, 8,8) << endl;
    cout << "gain/loss: " << (output->gains()<0?string("-"):"") << "$" << int_str(abs(output->gains()), 8, 8,8) << endl;
    cout << "exchange:  " << output->exchange << endl;
    cout << "tx ID:     " << output->txID << endl;
    cout << "notes:     " << output->notes << endl;
}

void editInput(Basis::Input* input)
{
    using namespace NumStr;
// date
    string dateStr = input->date.format("%Y-%M-%D %H:%I:%S");
    cout << "date: [" << dateStr << "]\nYYYYMMDDHHMMSS or YYYY-MM-DD HH:MM:SS>";
    getline(cin, dateStr);
    if (dateStr == "") {cout << "no change\n";}
    else if (!DateTime(dateStr).isValid()) {cout << "invalid date\n";}
    else {input->date = DateTime(dateStr);}
// amount
    string amountStr;
    switch (currentBasis.getPreferredUnit())
    {
    case Basis::enumBTCunit::BTC: amountStr = int_str(input->amount, 8, 8,8); break;
    case Basis::enumBTCunit::mBTC: amountStr = int_str(input->amount, 5, 5,5); break;
    case Basis::enumBTCunit::uBTC: amountStr = int_str(input->amount, 2, 2,2); break;
    case Basis::enumBTCunit::sat: amountStr = int_str(input->amount, 0, 0,0); break;
    }
    cout << "\namount: [" << amountStr << "] " << currentBasis.getPreferredUnitString() << "\nEnter in terms of " << currentBasis.getPreferredUnitString() << ">";
    getline(cin, amountStr);
    if (amountStr == "") {cout << "no change\n";}
    else if (amountStr.find_first_not_of("0123456789.") != string::npos) {cout << "invalid amount\n";} // check for negative amounts and invalid strings
    else if (amountStr.find('.') == string::npos) // if no fractional part, add trailing zeros if necessary
    {
        switch (currentBasis.getPreferredUnit())
        {
            case Basis::enumBTCunit::BTC: amountStr += "00000000"; break;
            case Basis::enumBTCunit::mBTC: amountStr += "00000"; break;
            case Basis::enumBTCunit::uBTC: amountStr += "00"; break;
            case Basis::enumBTCunit::sat: amountStr = amountStr; break;
        }
        input->amount = str_int(amountStr);
    }
    else
    {
        int zerosNeeded = 0;
        switch (currentBasis.getPreferredUnit())
        {
            case Basis::enumBTCunit::BTC: zerosNeeded = 8 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::mBTC: zerosNeeded = 5 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::uBTC: zerosNeeded = 2 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::sat: zerosNeeded = 0 - amountStr.substr(amountStr.find('.')+1).length(); break;
        }
        if (zerosNeeded > 0) amountStr += string(zerosNeeded, '0'); // add zeros to get right length
        else amountStr = amountStr.substr(0, amountStr.length()+zerosNeeded); // truncate if too long
        amountStr.replace(amountStr.find('.'), 1, "");
        input->amount = str_int(amountStr);
    }
// cost
    string costStr = int_str(input->cost, 8, 8,8);
    cout << "\ncost: $[" << costStr << "]\nEnter in terms of $>";
    getline(cin, costStr);
    if (costStr == "") {cout << "no change\n";}
    else if (costStr.find_first_not_of("$0123456789.") != string::npos) {cout << "invalid cost\n";}
    else
    {
        if (costStr.find('$') != string::npos) costStr.replace(costStr.find('$'), 1, "");
        if (costStr.find('.') == string::npos)
        {
            costStr += "00000000";
        }
        else
        {
            int zerosNeeded = 8 - costStr.substr(costStr.find('.')+1).length();
            if (zerosNeeded > 0) costStr += string(zerosNeeded, '0'); // add zeros if necessary
            else costStr = costStr.substr(0, costStr.length() + zerosNeeded); // otherwise truncate
            costStr.replace(costStr.find('.'), 1, "");
        }
        input->cost = str_int(costStr);
    }
// fee
    string feeStr = int_str(input->fee, 8, 8,8);
    cout << "\nfee: $[" << feeStr << "]\nEnter in terms of $>";
    getline(cin, feeStr);
    if (feeStr == "") {cout << "no change\n";}
    else if (feeStr.find_first_not_of("$0123456789.") != string::npos) {cout << "invalid fee\n";}
    else
    {
        if (feeStr.find('$') != string::npos) feeStr.replace(feeStr.find('$'), 1, "");
        if (feeStr.find('.') == string::npos)
        {
            feeStr += "00000000";
        }
        else
        {
            int zerosNeeded = 8 - feeStr.substr(feeStr.find('.')+1).length();
            if (zerosNeeded > 0) feeStr += string(zerosNeeded, '0'); // add zeros if necessary
            else feeStr = feeStr.substr(0, feeStr.length() + zerosNeeded); // otherwise truncate
            feeStr.replace(feeStr.find('.'), 1, "");
        }
        input->fee = str_int(feeStr);
    }
// exchange
    string exchangeStr = input->exchange;
    cout << "\nexchange: [" << exchangeStr << "]\n>";
    getline(cin, exchangeStr);
    if (exchangeStr == "") {cout << "no change\n";}
    else input->exchange = exchangeStr;
// transaction
    string transStr = input->txID;
    cout << "\ntransaction (txid): [" << transStr << "]\n>";
    getline(cin, transStr);
    if (transStr == "") {cout << "no change\n";}
    else if (transStr.find_first_not_of("0123456789abcdef") != string::npos) {cout << "invalid txid\n";}
    else input->txID = transStr;
// notes
    string notesStr = input->notes;
    cout << "\nnotes: [" << notesStr << "]\n>";
    getline(cin, notesStr);
    if (notesStr == "") {cout << "no change\n";}
    else {input->notes = notesStr;}
}

void editOutput(Basis::Output* output)
{
    using namespace NumStr;
// date
    string dateStr = output->date.format("%Y-%M-%D %H:%I:%S");
    cout << "date: [" << dateStr << "]\nYYYYMMDDHHMMSS or YYYY-MM-DD HH:MM:SS>";
    getline(cin, dateStr);
    if (dateStr == "") {cout << "no change\n";}
    else if (!DateTime(dateStr).isValid()) {cout << "invalid date\n";}
    else {output->date = DateTime(dateStr);}
// amount
    string amountStr;
    switch (currentBasis.getPreferredUnit())
    {
    case Basis::enumBTCunit::BTC: amountStr = int_str(output->amount, 8, 8,8); break;
    case Basis::enumBTCunit::mBTC: amountStr = int_str(output->amount, 5, 5,5); break;
    case Basis::enumBTCunit::uBTC: amountStr = int_str(output->amount, 2, 2,2); break;
    case Basis::enumBTCunit::sat: amountStr = int_str(output->amount, 0, 0,0); break;
    }
    cout << "\namount: [" << amountStr << "] " << currentBasis.getPreferredUnitString() << "\nEnter in terms of " << currentBasis.getPreferredUnitString() << ">";
    getline(cin, amountStr);
    if (amountStr == "") {cout << "no change\n";}
    else if (amountStr.find_first_not_of("0123456789.") != string::npos) {cout << "invalid amount\n";} // check that it is valid
    else if (amountStr.find('.') == string::npos) // if no fractional part, add trailing zeros if necessary
    {
        switch (currentBasis.getPreferredUnit())
        {
            case Basis::enumBTCunit::BTC: amountStr += "00000000"; break;
            case Basis::enumBTCunit::mBTC: amountStr += "00000"; break;
            case Basis::enumBTCunit::uBTC: amountStr += "00"; break;
            case Basis::enumBTCunit::sat: amountStr = amountStr; break;
        }
        output->amount = str_int(amountStr);
    }
    else
    {
        int zerosNeeded = 0;
        switch (currentBasis.getPreferredUnit())
        {
            case Basis::enumBTCunit::BTC: zerosNeeded = 8 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::mBTC: zerosNeeded = 5 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::uBTC: zerosNeeded = 2 - amountStr.substr(amountStr.find('.')+1).length(); break;
            case Basis::enumBTCunit::sat: zerosNeeded = 0 - amountStr.substr(amountStr.find('.')+1).length(); break;
        }
        if (zerosNeeded > 0) amountStr += string(zerosNeeded, '0'); // add zeros to get right length
        else amountStr = amountStr.substr(0, amountStr.length()+zerosNeeded); // truncate if too long
        amountStr.replace(amountStr.find('.'), 1, "");
        output->amount = str_int(amountStr);
    }
// proceeds
    string proceedsStr = int_str(output->proceeds, 8, 8,8);
    cout << "\nproceeds: $[" << proceedsStr << "]\nEnter in terms of $>";
    getline(cin, proceedsStr);
    if (proceedsStr == "") {cout << "no change\n";}
    else if (proceedsStr.find_first_not_of("$0123456789.") != string::npos) {cout << "invalid proceeds\n";}
    else
    {
        if (proceedsStr.find('$') != string::npos) proceedsStr.replace(proceedsStr.find('$'), 1, "");
        if (proceedsStr.find('.') == string::npos)
        {
            proceedsStr += "00000000";
        }
        else
        {
            int zerosNeeded = 8 - proceedsStr.substr(proceedsStr.find('.')+1).length();
            if (zerosNeeded > 0) proceedsStr += string(zerosNeeded, '0'); // add zeros if necessary
            else proceedsStr = proceedsStr.substr(0, proceedsStr.length() + zerosNeeded); // otherwise truncate
            proceedsStr.replace(proceedsStr.find('.'), 1, "");
        }
        output->proceeds = str_int(proceedsStr);
    }
// exchange
    string exchangeStr = output->exchange;
    cout << "\nexchange: [" << exchangeStr << "]\n>";
    getline(cin, exchangeStr);
    if (exchangeStr == "") {cout << "no change\n";}
    else output->exchange = exchangeStr;
// transaction
    string transStr = output->txID;
    cout << "\ntransaction (txid): [" << transStr << "]\n>";
    getline(cin, transStr);
    if (transStr == "") {cout << "no change\n";}
    else if (transStr.find_first_not_of("0123456789abcdef") != string::npos) {cout << "invalid txid\n";}
    else output->txID = transStr;
// notes
    string notesStr = output->notes;
    cout << "\nnotes: [" << notesStr << "]\n>";
    getline(cin, notesStr);
    if (notesStr == "") {cout << "no change\n";}
    else {output->notes = notesStr;}
}

void editOptions()
{
    string inputStr;
    cout << "\nUnits [" << currentBasis.getPreferredUnit() << "]\n" << Basis::listBTCUnits() << ">";
    char units = getCharInput();
    currentBasis.setPreferredUnit((Basis::enumBTCunit)NumStr::str_int(string(1, units)));
}

void exportToCSV()
{
    using NumStr::int_str;
    string csvFilename = "";
    cout << "\nWhat would you like to call the exported file? [form8949.csv]\n>";
    getline(cin, csvFilename);
    if (csvFilename == "") csvFilename = "form8949.csv";
    ofstream oFile(csvFilename);
    if (oFile.is_open())
    {
        oFile << "Description (a),Date acquired (b), Date disposed (c), Proceeds (d), Basis (e), Gain (h)\n";
        for (unsigned int i = 0; i < currentBasis.getNumOutputs(); i++)
        {
            Basis::Output* output = currentBasis.getOutput(i);
            string descStr;
            switch (currentBasis.getPreferredUnit())
            {
            case Basis::enumBTCunit::BTC: descStr = int_str(output->amount, 8, 8,8) + " " + currentBasis.getPreferredUnitString(); break;
            case Basis::enumBTCunit::mBTC: descStr = int_str(output->amount, 5, 5,5) + " " + currentBasis.getPreferredUnitString(); break;
            case Basis::enumBTCunit::uBTC: descStr = int_str(output->amount, 2, 2,2) + " " + currentBasis.getPreferredUnitString(); break;
            case Basis::enumBTCunit::sat: descStr = int_str(output->amount, 0, 0,0) + " " + currentBasis.getPreferredUnitString(); break;
            }
            string dateAcqStr;
            if (output->inputs.size() == 1) dateAcqStr = output->inputs[0].in->date.format("%Y/%M/%D");
            else dateAcqStr = "various";
            string dateDispStr = output->date.format("%Y/%M/%D");
            int64_t cost = output->proceeds - output->gains();
            oFile << descStr << "," <<
                     dateAcqStr << "," <<
                     dateDispStr << "," <<
                     int_str(output->proceeds, 8, 2,2) << "," <<
                     int_str(cost, 8, 2,2) << "," <<
                     int_str(output->gains(), 8, 2,2) <<
                     endl;

        }
        oFile.close();
    }
    cout << "Done\n";
    cout << "Generate full report on how " << softwareName << " is calculating gains? (y/N)\n>";
    char input = getCharInput();
    if (input == 'y') exportToVerboseCSV();
}

void exportToVerboseCSV()
{
    using NumStr::int_str;
    cout << "Name of file? [fullreport.csv]\n>";
    string csvFilename = "";
    getline(cin, csvFilename);
    if (csvFilename == "") csvFilename = "fullreport.csv";
    ofstream oFile(csvFilename);
    if (oFile.is_open())
    {
        oFile << "Output ID,Date,Amount,Input ID,Input Amount,Input Proceeds,Input Cost,Input Gain,Total Gain\n";
        for (unsigned int i = 0; i < currentBasis.getNumOutputs(); i++)
        {
            Basis::Output* output = currentBasis.getOutput(i);
            string oAmountStr = "";
            switch (currentBasis.getPreferredUnit())
            {
            case Basis::enumBTCunit::BTC: oAmountStr = int_str(output->amount, 8, 8,8); break;
            case Basis::enumBTCunit::mBTC: oAmountStr = int_str(output->amount, 5, 5,5); break;
            case Basis::enumBTCunit::uBTC: oAmountStr = int_str(output->amount, 2, 2,2); break;
            case Basis::enumBTCunit::sat: oAmountStr = int_str(output->amount, 0, 0,0); break;
            }
            oFile << output->id << "," <<
                     output->date.format("%Y/%M/%D %H:%I:%S") << "," <<
                     oAmountStr;
            for (unsigned int j = 0; j < output->inputs.size(); j++)
            {
                Basis::InputReference* inRef = &output->inputs[j];
                int64_t subProceeds = output->proceeds * (double)inRef->amount/output->amount;
                int64_t subCost = (inRef->in->cost + inRef->in->fee)*(double)inRef->amount/inRef->in->amount;
                int64_t subGain = subProceeds - subCost;
                string iAmountStr = "";
                switch (currentBasis.getPreferredUnit())
                {
                case Basis::enumBTCunit::BTC: iAmountStr = int_str(inRef->amount, 8, 8,8); break;
                case Basis::enumBTCunit::mBTC: iAmountStr = int_str(inRef->amount, 5, 5,5); break;
                case Basis::enumBTCunit::uBTC: iAmountStr = int_str(inRef->amount, 2, 2,2); break;
                case Basis::enumBTCunit::sat: iAmountStr = int_str(inRef->amount, 0, 0,0); break;
                }
                oFile << "," << inRef->in->id << "," <<
                         iAmountStr << "," <<
                         int_str(subProceeds, 8, 8,8) << "," <<
                         int_str(subCost, 8, 8,8) << "," <<
                         int_str(subGain, 8, 8,8);
                if (j == 0)
                {
                    oFile << "," << int_str(output->gains(), 8, 8,8) << endl;
                }
                else oFile << endl;
                if (j < output->inputs.size() - 1)
                {
                    oFile << ",,";
                }
            }
        }
        oFile.close();
    }
}

