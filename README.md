Crypto Basis
============

Easily track the value of your crypto over time.

Cryptobasis is a small CLI on top of my `Basis` class. With it you can track you incoming and outgoing bitcoins. You can generate a report stating your gains/losses for every output using the first in, first out method. This is useful for filling out tax forms and doing your finances "the right way" or the way "Uncle Sam" would want.

I've done my best to keep the code generic so that it can easily be modified to be used with any other cryptocurrency. In addition, the `Basis` class can be pulled from the project and implemented behind a **much better** GUI. This repo exists mostly as a proof of concept and testing ground for the `Basis` class.

For more information, please see [the wiki](https://bitbucket.org/pointychimp/cryptobasis/wiki/Home).

Warning
-------

This code assumes the user is pretty well behaved. There are some instances where errors are caught and handled nicely, but generally speaking, bad things happen when the user misbehaves.

* Save changes often
* Don't add outputs such that you have more going out than coming in (should be handled correctly in the future)
* Don't edit the data files directly.

Changelog
=========

* v1.3.1 [\[source\]](https://bitbucket.org/pointychimp/cryptobasis/commits/09e9615db45ddfc48af875495093a3444a5740ff): **Critical fix**. Fix reading output notes and exchange from file.
* v1.3 [\[source\]](https://bitbucket.org/pointychimp/cryptobasis/commits/2fb1a2cbeb831a0c05af687ec6c9d7a63d75e69b): Add verbose export to csv for seeing inner calculations
* v1.2 [\[source\]](https://bitbucket.org/pointychimp/cryptobasis/commits/9a0501dd5470d0137abd48a0790b24885e74c211): Add export to csv.
* v1.1 [\[source\]](https://bitbucket.org/pointychimp/cryptobasis/commits/6bae879ae9c5899d5cc38d44829ec061161a252f): Calculate and display gains.
* v1 [\[source\]](https://bitbucket.org/pointychimp/cryptobasis/commits/e5ebe58b7109e7aa98ec2d50f33dbb844b837223): First release. Shows and saves inputs and outputs correctly. Starting to get useful but not complete.

Todo
====

* append file extension when none entered
* license
* always: wiki

Binary downloads
================

[https://bitbucket.org/pointychimp/cryptobasis/downloads](https://bitbucket.org/pointychimp/cryptobasis/downloads)

Building from source
====================

* I apologize in advance: I have done very little compiling from the command line. Here's what I can tell you ...
* Requires C++11 (`-std=c++11`)
* Also requires my `namespace NumStr` and `class DateTime` (both included)


End
===

* Like this? Send me a few bitcents if you so desire: *1G47Gzieo22YaYmp41nXCwsMvQcAQVrAPN*.
* Problems/suggestions? [Submit an issue](https://bitbucket.org/pointychimp/cryptobasis/issues)

